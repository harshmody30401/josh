package com.harshmody.joshinternship;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import com.skydoves.balloon.ArrowOrientation;
import com.skydoves.balloon.Balloon;
import com.skydoves.balloon.BalloonAnimation;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {
    Button button1, button2, button3;
    Balloon balloon1, balloon2, balloon3, iconballoon1, iconballoon2, iconballoon3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        balloon1 = new Balloon.Builder(getApplicationContext())
                .setArrowSize(10)
                .setArrowOrientation(ArrowOrientation.LEFT)
                .setIsVisibleArrow(true)
                .setArrowPosition(0.3f)
                .setWidthRatio(0.6f)
                .setHeight(75)
                .setTextSize(15f)
                .setCornerRadius(4f)
                .setAlpha(0.9f)
                .setText("Yeti here, reporting for Crusher duty! What do ya need me to do, Tiffi?")
                .setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.fontColor))
                .setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.bgColor))
                .setBalloonAnimation(BalloonAnimation.OVERSHOOT)
                .build();
        iconballoon1 = new Balloon.Builder(getApplicationContext())
                .setIsVisibleArrow(false)
                .setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparentBg))
                .setIconDrawable(ContextCompat.getDrawable(getApplicationContext(), R.mipmap.tooltiplogo1_round))
                .setIconSize(50)
                .setBalloonAnimation(BalloonAnimation.OVERSHOOT)
                .build();
        balloon2 = new Balloon.Builder(getApplicationContext())
                .setArrowSize(10)
                .setArrowOrientation(ArrowOrientation.LEFT)
                .setIsVisibleArrow(true)
                .setArrowPosition(0.3f)
                .setWidthRatio(0.6f)
                .setHeight(75)
                .setTextSize(15f)
                .setCornerRadius(4f)
                .setAlpha(0.9f)
                .setText("Drag matching candies to make a row! 3 candies will make magic.")
                .setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.fontColor))
                .setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.bgColor))
                .setBalloonAnimation(BalloonAnimation.FADE)
                .build();
        iconballoon2 = new Balloon.Builder(getApplicationContext())
                .setIsVisibleArrow(false)
                .setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparentBg))
                .setIconDrawable(ContextCompat.getDrawable(getApplicationContext(), R.mipmap.tooltiplogo2_round))
                .setIconSize(50)
                .setBalloonAnimation(BalloonAnimation.FADE)
                .build();
        balloon3 = new Balloon.Builder(getApplicationContext())
                .setArrowSize(10)
                .setIsVisibleArrow(false)
                .setArrowPosition(0.3f)
                .setWidthRatio(0.9f)
                .setHeight(75)
                .setTextSize(15f)
                .setCornerRadius(4f)
                .setAlpha(0.9f)
                .setText("Collect blue candies to meet the goal for this level.")
                .setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.fontColor))
                .setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.bgColor))
                .setBalloonAnimation(BalloonAnimation.ELASTIC)
                .build();
        iconballoon3 = new Balloon.Builder(getApplicationContext())
                .setIsVisibleArrow(false)
                .setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparentBg))
                .setIconDrawable(ContextCompat.getDrawable(getApplicationContext(), R.mipmap.tooltiplogo3_round))
                .setIconSize(30)
                .setBalloonAnimation(BalloonAnimation.ELASTIC)
                .build();
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                balloon1.showAlignTop(button1, 35);
                iconballoon1.showAlignTop(button1, -235);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        balloon1.dismiss();
                        iconballoon1.dismiss();
                    }
                }, 3000);
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                balloon2.showAlignTop(button2, 35);
                iconballoon2.showAlignTop(button2, -235);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        balloon2.dismiss();
                        iconballoon2.dismiss();
                    }
                }, 3000);
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                balloon3.showAlignBottom(button3);
                iconballoon3.showAlignBottom(button3, 350, 20);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        balloon3.dismiss();
                        iconballoon3.dismiss();
                    }
                }, 3000);
            }
        });
    }

}